# Script de post installation pour serveurs CentOS 7

Ce dépôt contient un script de post-installation pour les serveurs
CentOS 7, ainsi qu'une série de scripts et de modèles de fichiers de 
configuration pour des services de base.

## En résumé

Suivre les actions suivantes.

 1. Installer un CentOS 7 version minimale.
 2. Créer un utilisateur non "root" avec privilèges administrateur.
 3. Installer Git : `sudo yum install git`
 4. Télécharger le script : `git clone https://gitlab.com/kikinovak/centos-7.git`
 5. Se rendre dans le répertoire : `cd centos-7`
 6. Executer le script : `sudo ./centos-setup.sh --setup`
 7. Faites-vous couler un café pendant que le script fait tout le travail !
 8. Redémarrer.

## Personnaliser un serveur CentOS

Transformer une installation miminale de CentOS en serveur fonctionnel se résume à une série d'opérations plus ou moins chronophages.

* Customiser le shell Bash : prompt, alias, etc.
* Customiser l'éditeur Vim.
* Installer les dépôts officiels et/ou tierces.
* Installer une suite complète d'outils en ligne de commande
* Supprimer les paquets non nécessaires.
* Autoriser l'administrateur à avoir accès aux logs système.
* Désactiver IPv6 et reconfigurer certains services en fonction.
* Configurer un mot de passe fort pour `sudo`.
* Etc.

Le script `centos-setup.sh`effectue toutes ces opérations. 

Configurer une résolution plus lisible par défaut pour Bash et Vim :

```
# ./centos-setup.sh --shell
```

Installer les dépôts officiels et tierces :

```
# ./centos-setup.sh --repos
```

Installer les paquets `Core` et `Base` en plus d'une série d'outils supplémentaires :

```
# ./centos-setup.sh --extra
```

Supprimer les paquets non essentiels :

```
# ./centos-setup.sh --prune
```

Autoriser l'administrateur à accéder aux logs système :

```
# ./centos-setup.sh --logs
```

Désactiver IPv6 et reconfigurer les services affectés :

```
# ./centos-setup.sh --ipv4
```

Configurer un mot de passe pour sudo :

```
# ./centos-setup.sh --sudo
```

Effectuer toutes les actions ci-dessus en une fois :

```
# ./centos-setup.sh --setup
```

Retirer les paquets installés et revenir au système de base :

```
# ./centos-setup.sh --strip
```

Afficher l'aide :

```
# ./centos-setup.sh --help
```

Afficher les logs :

```
$ tail -f /tmp/centos-setup.log
```